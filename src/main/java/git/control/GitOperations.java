package main.java.git.control;

import org.bouncycastle.openpgp.operator.PGPContentSignerBuilder;
import com.jcraft.jsch.*;
import org.eclipse.jgit.api.*;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.RefNotFoundException;
import org.eclipse.jgit.dircache.DirCache;
import org.eclipse.jgit.internal.storage.file.FileRepository;
import org.eclipse.jgit.lib.ConfigConstants;
import org.eclipse.jgit.transport.SshSessionFactory;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.transport.*;

import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;

public class GitOperations {

    String path = "";

    //PGPContentSignerBuilder pgp = new PGPContentSignerBuilder();

    static GitOperations gitOps = new GitOperations();
    CredentialsProvider cp; // For static members
    Git git;
    String DEFAULT_REMOTE_NAME = "origin";

    public FileRepositoryBuilder repositoryBuilder;
    static private String username = "artmu";
    static private String password = "F0xtr072008";

    public static void SetupAndCheckout(String username, String password, String directory, String branch)
            throws IOException, GitAPIException {
        System.out.println("### Setting up GIT, with user name: " + username + " password: " + password);
        gitOps.cp = new UsernamePasswordCredentialsProvider(username, password);

        Repository repository = new FileRepository(directory + "/.git");
        System.out.println("### Local repo path we are going to use: " + repository);
        gitOps.git = new Git(repository);

        cleanLocalRepo();

    }

    public static void cloneRepo(String path) {
        String repoUrl = "git@bitbucket.org:svg_lab/svg_automation.git";
        String cloneDirectoryPath = path; // Ex.in windows c:\\gitProjects\SpringBootMongoDbCRUD\
        try {
            System.out.println("Cloning " + repoUrl + " into " + repoUrl);
            Git.cloneRepository()
                    .setURI(repoUrl)
                    .setDirectory(Paths.get(cloneDirectoryPath).toFile())
                    .call();
            System.out.println("Completed Cloning");
        } catch (GitAPIException e) {
            System.out.println("Exception occurred while cloning repo");
            e.printStackTrace();
        }
    }

    private static void gitFetch() throws GitAPIException {
        System.out.println("### GIT fetch");
        ArrayList al = (ArrayList) gitOps.git.branchList().setListMode(ListBranchCommand.ListMode.ALL).call();

        System.out.println("### Response for list branches: " + al.toString());
        FetchResult fr = gitOps.git.fetch().setCredentialsProvider(gitOps.cp).call();
        System.out.println("### Response for fetch command: " + fr.toString());
    }

    public static void fetch(String localBranchName, String remoteBranchName) {

        RefSpec spec = new RefSpec().setSourceDestination(localBranchName, remoteBranchName);
        FetchCommand command = gitOps.git.fetch();
        command.setRefSpecs(spec);
        FetchResult result = null;
        try {
            result = command.call();
        } catch (Throwable e) {
            throw new RuntimeException(String.format(
                    "Failed to fetch from [%s] to [%s]",
                    remoteBranchName,
                    localBranchName), e);
        }
        System.out.println("### Result of fetch: " + result.getMessages());
    }

    private static void cleanLocalRepo() throws GitAPIException {
        System.out.println("### Clean local repository, git stash create");
        // NOTE stash create does not save stash anywhere so we let to loose it
        try {
            if(!gitOps.git.status().call().isClean()) {
                StashCreateCommand scc = gitOps.git.stashCreate();
                RevCommit rc = scc.call();
                System.out.println("### Stash create response: " + rc.toString());
            }
            else{
                System.out.println("### No need for GIT stash");
            }
        } catch (Throwable e) {
            System.out.println("### Stashing did not worked, " + e.toString());
            gitOps.git.close();
        }


    }

    public static void gitStatus() throws GitAPIException {
        System.out.println("### GIT status");
        StatusCommand command = gitOps.git.status();

        Status rc = command.call();
        System.out.println("### Response for Status - conflicting: " + rc.getConflicting().toString());
        System.out.println("### Response for Status - changed    : " + rc.getChanged().toString());

    }

    public static void gitPull(String branchName) throws GitAPIException {
        System.out.println("### GIT checkout and pull branch: " + branchName);
        try {
            // Lets try first, if no success we try with Fetch in catch
            System.out.println("### Trying to GIT checkout in to branch: " + branchName);
            gitOps.git.checkout().setName(branchName).call();
            PullCommand pullCommand = gitOps.git.pull();
            PullResult pr = pullCommand.setCredentialsProvider(gitOps.cp).call();;
            //PullResult pr = gitOps.git.pull().setCredentialsProvider(gitOps.cp).setRemote("origin").call();
            System.out.println("### Response for GIT pull: " + pr.toString());
            gitOps.git.close();
        } catch (RefNotFoundException e) {
            System.out.println("Branch not found, checkout failed, trying again with GIT fetch for branch " + branchName);
            gitOps.git.fetch().setCredentialsProvider(gitOps.cp).call();
            gitOps.git.checkout()
                    .setCreateBranch(false)
                    .setName(branchName)
                    .setUpstreamMode(CreateBranchCommand.SetupUpstreamMode.TRACK)
                    .setStartPoint(gitOps.DEFAULT_REMOTE_NAME + "/" + branchName).
                    call();
            gitOps.git.pull().setCredentialsProvider(gitOps.cp).call();
            // TODO in some internet discussions they support to close always in some no?
            gitOps.git.close();
        }
    }

    public static void gitStage(Path directory) throws IOException, InterruptedException, GitAPIException {
        System.out.println("### GIT staging");
        AddCommand cmd = gitOps.git.add();
        cmd.addFilepattern("unit_configs/");
        DirCache response = cmd.call();
        System.out.println("### Stage Response: " + response.toString());
    }

    public static void gitStage2(Path directory, String user) throws GitAPIException {
        AddCommand addCommand = gitOps.git.add()
                .addFilepattern(".")
                .addFilepattern("unit_configs/*.json");
        addCommand.call();
    }

    public static void gitCommit(Path directory, String message) throws GitAPIException {
        System.out.println("### GIT commit");
        RevCommit rc = gitOps.git.commit().setAll(false).setMessage(message).call();
        System.out.println("### Commit response: " + rc.toString());
    }

    public static void gitPush(String directory) throws IOException, InterruptedException, GitAPIException {
        System.out.println("### GIT push");
        PushCommand pushCommand = gitOps.git.push();
        pushCommand.setRemote("origin");
        pushCommand.setRefSpecs(new RefSpec(directory));
        Iterable<PushResult> response = pushCommand.setCredentialsProvider(gitOps.cp).call();
        
        for (PushResult pushResult : response) {
            System.out.println("Result of pushing to remote: " + pushResult.getRemoteUpdates());
        }

    }

    // NOTE Not in use currently - old implementation
    public static void runCommand(Path directory, String... command) throws IOException, InterruptedException {
        Objects.requireNonNull(directory, "directory");
        if (!Files.exists(directory)) {
            throw new RuntimeException("can't run command in non-existing directory '" + directory + "'");
        }
        ProcessBuilder pb = new ProcessBuilder()
                .command(command)
                .directory(directory.toFile());
        Process p = pb.start();
        StreamGobbler errorGobbler = new StreamGobbler(p.getErrorStream(), "ERROR");
        StreamGobbler outputGobbler = new StreamGobbler(p.getInputStream(), "OUTPUT");
        outputGobbler.start();
        errorGobbler.start();
        int exit = p.waitFor();
        errorGobbler.join();
        outputGobbler.join();
        if (exit != 0) {
            throw new AssertionError(String.format("runCommand returned %d", exit));
        }
    }

    private static class StreamGobbler extends Thread {

        private final InputStream is;
        private final String type;

        private StreamGobbler(InputStream is, String type) {
            this.is = is;
            this.type = type;
        }

        @Override
        public void run() {
            try (BufferedReader br = new BufferedReader(new InputStreamReader(is));) {
                String line;
                while ((line = br.readLine()) != null) {
                    System.out.println(type + "> " + line);
                }
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        GitOperations go = new GitOperations();
        Path localRepoPath = Paths.get("/home/arto/work/svg_automation");
        //GitOperations.cloneRepo("/home/arto/work/temp");
       // GitOperations.gitPull(localRepoPath, "java_json_updater_exercises");
        //GitOperations.gitStage(localRepoPath);
        //GitOperations.gitCommit(localRepoPath, "Updating JSONs");
        //GitOperations.gitPush(localRepoPath);
    }
}