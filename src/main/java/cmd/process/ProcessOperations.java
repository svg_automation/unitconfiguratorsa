package main.java.cmd.process;

import java.io.BufferedInputStream;
import java.io.IOException;

public class ProcessOperations {

    private ProcessBuilder processBuilder;
    static ProcessOperations proc = new ProcessOperations();

    public static void runScript(String scriptPath, String json_path) throws IOException, InterruptedException {
        // NOTE !!! Windows Binary pscp.exe needs to be same folder as the UnitConfiguratorSA.jar !!!
        if (System.getProperty("os.name").contains("Linux")) {
            System.out.println("### OS is Linux");
            proc.processBuilder = new ProcessBuilder("bash", scriptPath, json_path);
        } else {
            // Works actually only in Windows
            System.out.println("### OS something else than Linux");
            proc.processBuilder = new ProcessBuilder("pscp", "-scp", "c:\\work\\UnitConfiguratorJAR\\text.txt", "test.json");
        }

        // Sets the source and destination for subprocess standard I/O to be the same as those of the current Java process.
        proc.processBuilder.inheritIO();
        Process process = proc.processBuilder.start();

        int exitValue = process.waitFor();
        if (exitValue != 0) {
            // check for errors
            new BufferedInputStream(process.getErrorStream());
            throw new RuntimeException("execution of script failed! Exception value: " + exitValue);
        }
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        ProcessOperations.runScript("/home/arto/work/Projects/Java/UnitConfigUpdater/src/scripts/sync_jsons_in_server.sh", "/home/arto/work/svg_automation");
    }
}
