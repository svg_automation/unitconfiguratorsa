package main.java.cmd.ssh;

//import com.jcraft.jsch.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class SshCommands {

//    Session session = null;
//    Channel channel = null;
//
//    String password = "localadmin";
//    String remoteFilename = "/var/lib/jenkins/userContent/hardware_data_v1.json";
//    String localFilename = "hardware_data_v1.json";
//    String host = "10.75.26.151";
//
//    public void setupSsh() throws JSchException, IOException {
//
//        JSch jsch = new JSch();
//        String username = "jenkins";
//        String host = "10.75.26.151";
//        session = jsch.getSession(username, host, 22);
//        java.util.Properties config = new java.util.Properties();
//        config.put("StrictHostKeyChecking", "no");
//        session.setConfig(config);
//        session.setPassword(password);
//        session.connect();
//
//// exec 'scp -f rfile' remotely
//        String command = "scp -f " + " " + host + ":" + remoteFilename;
//        channel = session.openChannel("exec");
//        ((ChannelExec) channel).setCommand(command);
//
//// get I/O streams for remote scp
//        OutputStream out = channel.getOutputStream();
//        InputStream in = channel.getInputStream();
//
//        channel.connect();
//
//        byte[] buf = new byte[1024];
//
//// send '\0'
//        buf[0] = 0;
//        try {
//            out.write(buf, 0, 1);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        try {
//            out.flush();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        while (true) {
//            int c = 0;
//            try {
//                c = checkAck(in);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            if (c != 'C') {
//                break;
//            }
//
//            // read '0644 '
//            try {
//                in.read(buf, 0, 5);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//
//            long filesize = 0L;
//            while (true) {
//                try {
//                    if (in.read(buf, 0, 1) < 0) {
//                        // error
//                        break;
//                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                if (buf[0] == ' ') {
//                    break;
//                }
//                filesize = filesize * 10L + (long) (buf[0] - '0');
//            }
//
//            String file = null;
//            for (int i = 0;; i++) {
//                try {
//                    in.read(buf, i, 1);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                if (buf[i] == (byte) 0x0a) {
//                    file = new String(buf, 0, i);
//                    break;
//                }
//            }
//
//            // send '\0'
//            buf[0] = 0;
//            try {
//                out.write(buf, 0, 1);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            try {
//                out.flush();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//
//            // read a content of lfile
//            FileOutputStream fos = null;
//
//            fos = new FileOutputStream(localFilename);
//            int foo;
//            while (true) {
//                if (buf.length < filesize) {
//                    foo = buf.length;
//                } else {
//                    foo = (int) filesize;
//                }
//                try {
//                    foo = in.read(buf, 0, foo);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                if (foo < 0) {
//                    // error
//                    break;
//                }
//                try {
//                    fos.write(buf, 0, foo);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                filesize -= foo;
//                if (filesize == 0L) {
//                    break;
//                }
//            }
//            try {
//                fos.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            fos = null;
//
//            try {
//                if (checkAck(in) != 0) {
//                    System.exit(0);
//                }
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//
//            // send '\0'
//            buf[0] = 0;
//            try {
//                out.write(buf, 0, 1);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            try {
//                out.flush();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//
//            channel.disconnect();
//            session.disconnect();
//        }
//
//        channel.disconnect();
//        session.disconnect();
//    }

    private static int checkAck(InputStream in) throws IOException {
        // b may be 0 for success
        //          1 for error,
        //          2 for fatal error,
        //          -1
        int b=in.read();
        if(b==0) return b;
        else if(b==-1) return b;
        if(b==1 || b==2) {
            StringBuilder sb=new StringBuilder();
            int c;
            do {
                c=in.read();
                sb.append((char)c);
            } while(c!='\n');
            throw new IOException(sb.toString());
        }
        return b;
    }


    /**
     * Parse key=value pairs to hashmap.
     * @param args
     * @return
     */
    private static Map<String,String> parseParams(String[] args) throws Exception {
        Map<String,String> params = new HashMap<String,String>();
        for(String keyval : args) {
            int idx = keyval.indexOf('=');
            params.put(
                    keyval.substring(0, idx),
                    keyval.substring(idx+1)
            );
        }
        return params;
    }

    private static String filenameHack(String filename) {
        // It's difficult reliably pass unicode input parameters
        // from Java dos command line.
        // This dirty hack is my very own test use case.
        if (filename.contains("${filename1}"))
            filename = filename.replace("${filename1}", "Korilla ABC ÅÄÖ.txt");
        else if (filename.contains("${filename2}"))
            filename = filename.replace("${filename2}", "test2 ABC ÅÄÖ.txt");
        return filename;
    }

}

