package main.java.unit.editor;


import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;


public class MainFrame extends JFrame {


    private HashMap<String, String> hmap = new HashMap<String, String>();

    public MainFrame() throws ParseException, IOException {
        super("Unit Configurator");
        JFrame frame = new JFrame("JOCK ver 1.1.1");
        frame.setContentPane(new JsonParserUI().createUIComponents());
        frame.setPreferredSize(new Dimension(1000, 400));
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setTitle("JSON Configurator version: 1.1.1");
    }

}