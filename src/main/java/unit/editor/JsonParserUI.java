package main.java.unit.editor;

import main.java.git.control.GitOperations;
import org.eclipse.jgit.api.errors.GitAPIException;

import javax.swing.*;
import javax.swing.filechooser.FileSystemView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

public class JsonParserUI {
    private JButton SetupLocalEnvButton;
    private JButton basicButton;
    private JButton detailedButton;
    private JButton pickFileUpdateButton;
    private JComboBox detailedComboBox;
    private JComboBox<String> basicComboBox;
    private JTextField texFieldDetailed;
    private JPanel uiParserPane;
    private JPanel MainPane;
    private JButton closeButton;
    private JButton commitChangesButton;
    private JTextField branchTextField;
    private JTextField userNameTxtField;

    private JPasswordField passwTextField;
    private JFileChooser fileChooser;
    private JLabel detailsLabel;
    private JLabel CommmitExpalanation;

    private Parser parser = new Parser();
    public static String repoDirPath = "";
    public static File repoDirPathFile;

    public static File json_file;
    public Map<String, String> chosenJsonChild;
    public String detailedChosenContent = "Nothing chosen";
    public static String parentKey;
    public static String childKey;
    public static String givenNewValue;
    public static String pathToRepo;
    public static Path localRepoPath;

    // TODO most of the methods can be moved to other classes: Parser etc ...
    public JPanel createUIComponents() throws IOException, ParseException {
        // TODO: place custom component creation code here
        fileChooser = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());

        SetupLocalEnvButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                System.out.println("### Setting up local environment for GIT");
                fileChooser = new JFileChooser();
                fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                int returnValue = fileChooser.showOpenDialog(null);
                repoDirPathFile = fileChooser.getSelectedFile();
                System.out.println("### Path to local repository dir: " + repoDirPathFile.toString());
                // repoDirPath is used in
                repoDirPath =  repoDirPathFile.toString();
                try {
                    parser.gitPathSetupAndPull(userNameTxtField, passwTextField, branchTextField);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (GitAPIException e) {
                    e.printStackTrace();
                }
                // File update button inactive until GIT environment set up and folder chosen
                pickFileUpdateButton.setEnabled(true);
            }
        });
        closeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.out.println("### Closing app ...");
                System.exit(0);
            }
        });

        detailedButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                detailedChosenContent = detailedComboBox.getSelectedItem().toString();
                System.out.println("### Detail choose: " + detailedChosenContent);
                System.out.println("### Detail key: " + detailedChosenContent.substring(0,
                        detailedChosenContent.indexOf(':')));
                childKey = detailedChosenContent.substring(0,
                        detailedChosenContent.indexOf(':')).replace("\"", "");
                System.out.println("### Detail value: " + detailedChosenContent.substring(
                        detailedChosenContent.indexOf(':') + 1));
                givenNewValue = detailedChosenContent.substring(
                        detailedChosenContent.indexOf(':')).replace("\"", "");
                givenNewValue = givenNewValue.replace(":", "");
                givenNewValue = givenNewValue.trim();
                texFieldDetailed.setText(givenNewValue);

                commitChangesButton.setEnabled(true);
            }

        });

        pickFileUpdateButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                {   // We have chosen the folder for this session we set it inactive
                    SetupLocalEnvButton.setEnabled(false);
                    // If we choose other file in same session ComboBox need to be empty from old stuff
                    basicComboBox.removeAllItems();
                    fileChooser = new JFileChooser(repoDirPath);
                    int returnValue = fileChooser.showOpenDialog(null);
                    if (returnValue == JFileChooser.APPROVE_OPTION) {
                        json_file = fileChooser.getSelectedFile();
                        System.out.println("### File selected: " + json_file.getAbsolutePath());
                        try {
                            parser.populateMap(basicComboBox);
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                        // NOTE: this need to be located after file is chosen successfully
                        basicButton.setEnabled(true);
                        detailedButton.setEnabled(true);
                        basicButton.addActionListener(new ActionListener() {
                            public void actionPerformed(ActionEvent e) {
                                String strChosenLine = basicComboBox.getSelectedItem().toString();
                                System.out.println("### Selected content: " + strChosenLine);
                                parentKey = strChosenLine.substring(0, strChosenLine.indexOf(':'));
                                System.out.println("### Selected Key: " + parentKey);
                                // TODO better parsing
                                int start_idx = strChosenLine.indexOf("{") + 1;
                                int end_idx = strChosenLine.indexOf("}");
                                String parsedContent = strChosenLine.substring(start_idx, end_idx);
                                System.out.println("### Parsed value " + parsedContent);
                                chosenJsonChild = parser.updateDetailedCombo(parsedContent, detailedComboBox);
                                // TODO chosenJsonChild is useless, it's same as parsedContent
                            }
                        });
                    }
                }
            }
        });

        commitChangesButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                int confirmation = -1;
                confirmation = JOptionPane.showConfirmDialog(null,
                            "Are you sure you are going to Commit?", "Commit confirmation",
                            JOptionPane.YES_NO_CANCEL_OPTION);
                System.out.println("### Dialog response: " + confirmation);
                // Set buttons inactive for new change inside same session
                basicButton.setEnabled(false);
                detailedButton.setEnabled(false);
                commitChangesButton.setEnabled(false);
                if(confirmation == 0) {
                    try {
                        System.out.println("### Commit confirmed on dialog");
                        parser.pickChange(texFieldDetailed);
                        parser.updateJsonWithTheChange();
                        GitOperations.gitStage(localRepoPath);
                        GitOperations.gitCommit(localRepoPath, "JSON updated");
                        GitOperations.gitPush(branchTextField.getText());

                    } catch (IOException | InterruptedException | GitAPIException ex) {
                        ex.printStackTrace();
                    }
                }
                else{
                    System.out.println("### Not going to commit now!!!");
                }

            }
        });
        return uiParserPane;
    }

}
