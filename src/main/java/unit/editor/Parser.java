package main.java.unit.editor;

import javax.swing.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.*;

import main.java.cmd.process.ProcessOperations;
import main.java.git.control.GitOperations;
import org.apache.commons.io.FileUtils;
import org.bouncycastle.jce.provider.JDKDSASigner;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class Parser {

    public Map<String, String> updateDetailedCombo(String keyValuepairStr, JComboBox detailedComboBox) {
        System.out.println("### Key Value Pairs: "  + keyValuepairStr);
        HashMap<String, String> hmap = new HashMap<String, String>();
    // New implementation
        detailedComboBox.removeAllItems();
        hmap = createKeyValueMap(keyValuepairStr);
        for (Map.Entry<String, String> entry : hmap.entrySet()) {
            System.out.println(entry.getKey() + " = " + entry.getValue());
            detailedComboBox.addItem(entry.getKey() + ":" + entry.getValue());
        }
    // Old way - works only for hardware_data_for_dev.json
//        String[] keyValuePairs = keyValuepairStr.split(",");
//
//        detailedComboBox.removeAllItems();
//        for(String pair : keyValuePairs)                    // iterate over the pairs
//        {
//            System.out.println("### Pair: " + pair);
//            String[] entry = pair.split(":");        // split the pairs to get key and value
//            hmap.put(entry[0].trim(), entry[1].trim());     // add them to the hashmap and trim whitespaces
//            detailedComboBox.addItem(entry[0].trim() + ": " + entry[1].trim());
//        }
//        System.out.println("### HashMap: " + hmap);
        // Redundant return
        return hmap;

    }

    public void emptyComboBox(JComboBox comboBox) {
        System.out.println("### Emptying ComboBox");
        comboBox.removeAllItems();
    }

    public void populateMap(JComboBox basicComboBox) throws IOException {
        JSONObject jsonObject = readJson(JsonParserUI.json_file);
        Iterator<String> keys = jsonObject.keys();
        List list;
        while(keys.hasNext()) {
            String key = keys.next();
            if (jsonObject.get(key) instanceof JSONObject) {
                String value = ((JSONObject) jsonObject.get(key)).toString();

                System.out.println("### JsonObj key:" + key);
                // If value is too long we just shrink it on the list view
                // if(value.length() > 120) value = value.substring(0,120) + "<br/>" + value.substring(121);
                System.out.println("### JsonObj value: " + value);
                basicComboBox.addItem(key + ": " + value);
            }
        }
    }

    public JSONObject readJson(File json_file) throws IOException {
        System.out.println("### Read JSON file: " + json_file);
        InputStream is = new FileInputStream(json_file);
        JSONTokener tokener = new JSONTokener(is);

        return new JSONObject(tokener);
    }

    public void updateJsonWithTheChange() throws IOException{
        // TODO deprecated method

        System.out.println("### updateJson(): Read JSON file: " + JsonParserUI.json_file);
        InputStream is = new FileInputStream(JsonParserUI.json_file);

        JSONTokener tokener = new JSONTokener(is);
        JSONObject obj = new JSONObject(tokener);
        // case we want to update with a list
        if (JsonParserUI.givenNewValue.indexOf(',') == -1){
            //                  Parent          Child
            obj.getJSONObject(JsonParserUI.parentKey).put(JsonParserUI.childKey, JsonParserUI.givenNewValue);
        }
        else {
            JsonParserUI.givenNewValue = JsonParserUI.givenNewValue.replace("[", "");
            JsonParserUI.givenNewValue = JsonParserUI.givenNewValue.replace("]", "");
            String[] lst = JsonParserUI.givenNewValue.split(",");
            obj.getJSONObject(JsonParserUI.parentKey).put(JsonParserUI.childKey, lst);
        }

        System.out.println("### Updating with the object: " + obj.toString());
        FileUtils.write(JsonParserUI.json_file, obj.toString(4));
        System.out.println("### JSON file updated");
    }

    public void pickChange(JTextField texFieldDetailed){
        System.out.println("### Picking changed value from text - field");
        String textInTextField = texFieldDetailed.getText();
        // detailedChosenContent = detailedComboBox.getSelectedItem().toString();
        System.out.println("### Picking value from text field content: " + textInTextField);
        System.out.println("### Detail value: " + textInTextField.substring(
                textInTextField.indexOf(':') + 1));
        JsonParserUI.givenNewValue = textInTextField.substring(
                textInTextField.indexOf(':') + 1).replace("\"", "");
    }

    public void gitPathSetupAndPull(JTextField userNameTextField, JTextField passwTextField, JTextField branchTextField) throws IOException, InterruptedException, GitAPIException {
        System.out.println("### Setup GIT path, pull fresh " + branchTextField + " branch");
        int endIndexToPath = JsonParserUI.repoDirPathFile.getAbsolutePath().indexOf("svg_automation") + 14;
        JsonParserUI.pathToRepo = JsonParserUI.repoDirPathFile.getAbsolutePath().substring(0, endIndexToPath);
        System.out.println("### Local repo path: " + JsonParserUI.pathToRepo);
        JsonParserUI.localRepoPath = Paths.get(JsonParserUI.pathToRepo);

        GitOperations.SetupAndCheckout(userNameTextField.getText(), passwTextField.getText(),
                JsonParserUI.pathToRepo, branchTextField.getText());
        GitOperations.gitStatus();
        GitOperations.gitPull(branchTextField.getText());
    }

    public void syncJSONsOnServer(String scriptPath, String jsonsPath) throws IOException, InterruptedException {
        System.out.println("### Syncing JSON changes to server, current path: " + System.getProperty("user.dir"));
        if (System.getProperty("os.name").contains("Linux")) {
            System.out.println("### Linux");
        } else {
            System.out.println("### ");
        }
        String currentPath = System.getProperty("user.dir");
        scriptPath = currentPath.substring(0, currentPath.indexOf("UnitConfiguratorSA") + 18) + "/src/scripts/sync_jsons_in_server.sh";
        System.out.println("### New Script path: " + scriptPath);
        System.out.println("### Syncing JSONs script path: " + scriptPath + " JSONs path " + jsonsPath);
        ProcessOperations.runScript(scriptPath, jsonsPath);
    }

    public HashMap<String, String> createKeyValueMap(String str) {
        System.out.println("### Creating Key - Value map from String content: " + str);
        HashMap<String, String> keyValuePairs = new HashMap<String, String>();

        String[] lst = str.split(":");
        System.out.println("### List: " + Arrays.toString(lst));

        String quoteAndComma = "\",";
        String VALUE;
        int j = 1;
        for (int i = 0; i < lst.length - 1; i++) {
            String KEY = lst[i];
            if (KEY.split(",").length == 1) {
                System.out.println("### No need to do anything, KEY " + KEY);
            } else if (KEY.contains("[")) {
                KEY = KEY.split("]")[1].replace(",", "");
            } else {
                KEY = KEY.split(",")[1];
            }
            KEY = KEY.replace(quoteAndComma, "").replace("\"", "");
            System.out.println("## Key: " + KEY);

            if (lst[i + 1].contains("[")) {
                VALUE = lst[i + 1].substring(0, lst[i + 1].indexOf("]") + 1);
            } else {
                // If we deal with normal divider of key - value pairs
                if (lst[i + 1].contains("\",")) {
                    VALUE = lst[i + 1].substring(0, lst[i + 1].indexOf(","));
                }
                // If comma ',' is inside value as between IP and amari: "Donor_Info":"10.77.3.22, amari"
                else {
                    VALUE = lst[i + 1];
                }
            }
            keyValuePairs.put(KEY, VALUE);
            VALUE = VALUE.replace("\"", "");
            System.out.println("## Value: " + VALUE);
        }
        return keyValuePairs;
    }
}
