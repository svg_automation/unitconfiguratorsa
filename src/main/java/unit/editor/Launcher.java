package main.java.unit.editor;

import javax.swing.*;
import java.io.IOException;
import java.text.ParseException;

public class Launcher {

    public static void main(String[] args) {

        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                System.out.println("### Launcher current dir: " + System.getProperty("user.dir"));
                try {
                    new MainFrame();
                } catch (ParseException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
