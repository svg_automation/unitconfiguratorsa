#!/usr/bin/env bash
# To get password free access from slave to JSON container server (Jenkins master)
# ~/.ssh/id_rsa.pub content needs to be copied to Jenkins master .ssh/known_hosts and authorized_keys files
# confluence-il:8090/display/SVGUK/Jenkins+setup+notes
BASE_PATH=$1
#BASE_PATH=/home/arto/work/svg_automation

echo "### $BASE_PATH"
echo "### Compare images.json server current version for local version which will overwrite server one ###"
ssh jenkins@10.75.26.151 "cat /var/lib/jenkins/userContent/images.json" | diff - "$BASE_PATH/"unit_configs/images.json
echo
echo "### Compare credentials.json server current version for local version which will overwrite server one ###"
ssh jenkins@10.75.26.151 "cat /var/lib/jenkins/userContent/credentials.json" | diff - "$BASE_PATH/"unit_configs/credentials.json
echo
echo "### Compare unit_basics.json server current version for local version which will overwrite server one ###"
ssh jenkins@10.75.26.151 "cat /var/lib/jenkins/userContent/unit_basics.json" | diff - "$BASE_PATH/"unit_configs/unit_basics.json
echo
echo "### Compare hardware_data_v1.json server current version for local version which will overwrite server one ###"
ssh jenkins@10.75.26.151 "cat /var/lib/jenkins/userContent/hardware_data_v1.json" | diff - "$BASE_PATH/"unit_configs/hardware_data_v1.json

echo "### Copying all JSONs from Suite to userContent - directory on server"
scp -r "$BASE_PATH/"unit_configs/test.json jenkins@10.75.26.151:/var/lib/jenkins/userContent/test.json